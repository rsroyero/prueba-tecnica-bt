####### Guia de Ejecución #########
1. Ubicarse en el directorio donde se encuentra ubicado el proyecto por medio de la consola de comando
    {Directorio}\tienda\tienda\tienda\tienda
2. Ejecutar el comando
	2.1 mvn clean 
	2.3 mvn spring-boot:run (Para Ejecutar el Aplicativo)
Tambien se pude importar el proyecto en eclipse y ejecutandolo con el plugin de spring-boot
3. Ingresar por la siguiente URL http://localhost:8080/index.html

########## Credenciales para la BD ###########
passwoord: root
user: root

######### Cambio Propiedades BD ##############
1. Ubicarse en el directorio donde se encuentra ubicado el proyecto
    {Directorio}\tienda\tienda\tienda\tienda\src\main\resources
2. Abrir el Arhivo application.properties
3. Modificar los datos de conexion
   spring.datasource.url=jdbc:mysql://localhost/tienda
   spring.datasource.username=root
   spring.datasource.password=root
