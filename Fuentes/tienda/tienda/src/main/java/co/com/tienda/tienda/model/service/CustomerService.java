package co.com.tienda.tienda.model.service;

import java.util.List;

import co.com.tienda.tienda.model.beans.entity.Customer;

public interface CustomerService {
	
	Customer findCustomerById(long customerId);
	
	List<Customer> findAllCustomers();

}
