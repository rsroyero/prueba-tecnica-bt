package co.com.tienda.tienda.web.controller;

import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import co.com.tienda.tienda.model.beans.dto.PurchaseOrderRecordDTO;
import co.com.tienda.tienda.model.beans.dto.PurchaseOrderReportDTO;
import co.com.tienda.tienda.model.beans.dto.PurchaseProduct;
import co.com.tienda.tienda.model.beans.entity.Customer;
import co.com.tienda.tienda.model.beans.entity.Product;
import co.com.tienda.tienda.model.service.CustomerService;
import co.com.tienda.tienda.model.service.ProductService;
import co.com.tienda.tienda.model.service.PurchaseOrderService;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class PrincipalOrderRestController {
	
	@Autowired
	private CustomerService customerService;
	@Autowired
	private ProductService productrService;
	@Autowired
	private PurchaseOrderService purchaseOrderService;
	
	
    @GetMapping("/all-customers")
    public ResponseEntity<List<Customer>> findAllCustomers() {
    	
    	try {
    		List<Customer> customerList = customerService.findAllCustomers();
        	if(customerList.isEmpty()){
    			return new ResponseEntity<List<Customer>>(HttpStatus.NOT_FOUND);
    		}
    		
    		return new ResponseEntity<List<Customer>>(customerList,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<Customer>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

    }
    
    @GetMapping("/all-products-customer/{customerId}")
    public ResponseEntity<List<Product>> findAllProductsByCustomer(@PathVariable("customerId") Long customerId) {
    	try {
    		Customer customer = customerService.findCustomerById(customerId);
    		List<Product> productList=productrService.findProductsByCustomer(customer);
    		if(productList.isEmpty()) {
    			return new ResponseEntity("No hay Productos disponibles para el usuario con id:"+customerId,HttpStatus.NOT_FOUND);
    		}
    		return new ResponseEntity<List<Product>>(productList,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<Product>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    
    @PostMapping("/purchase-order")
    public ResponseEntity<?> createPurchaseOrder(@RequestBody PurchaseOrderRecordDTO purchaseOrderRecordDTO, 
    									 		 UriComponentsBuilder ucBuilder){
    	try {
    		if(purchaseOrderRecordDTO==null) {
    			return new ResponseEntity<String>("Error al procesar la orden de compra",HttpStatus.CONFLICT);
    		}
    		
    		if(purchaseOrderRecordDTO.getProductList()!=null && !purchaseOrderRecordDTO.getProductList().isEmpty()) {
    			int amount = 0;
    			for (PurchaseProduct purchaseProduct: purchaseOrderRecordDTO.getProductList()) {
    				amount = amount + purchaseProduct.getProductAmount();
    				if(amount>5) {
    					return new ResponseEntity<String>("Limete de productos superado",HttpStatus.CONFLICT);
    				}
				}
    		}
    		
        	purchaseOrderService.createPurchaseOrder(purchaseOrderRecordDTO);
        	return new ResponseEntity<Void>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

    	
    }
    
    @GetMapping("/purchase-order-report/{customerId}/{beforeDate}/{afterDate}")
    public ResponseEntity<List<PurchaseOrderReportDTO>> findPurchaseOrderReport(@PathVariable("customerId") Long customerId, 
									    		@PathVariable("beforeDate")  String beforeDate,
									    		@PathVariable("afterDate")   String afterDate) {
    	try {

    		 SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

    		List<PurchaseOrderReportDTO> purchaseOrderReportDTOList=purchaseOrderService.findPurchaseOrderReport(customerId, 
    																											 format.parse(beforeDate), 
    																											 format.parse(afterDate));
    		if(purchaseOrderReportDTOList.isEmpty()) {
    			return new ResponseEntity<List<PurchaseOrderReportDTO>>(HttpStatus.NOT_FOUND);
    		}
    		
    		return new ResponseEntity<List<PurchaseOrderReportDTO>>(purchaseOrderReportDTOList,HttpStatus.OK); 
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<PurchaseOrderReportDTO>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	

    }

}
