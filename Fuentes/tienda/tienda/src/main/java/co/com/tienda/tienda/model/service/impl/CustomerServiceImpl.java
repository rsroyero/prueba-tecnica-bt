package co.com.tienda.tienda.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.tienda.tienda.model.beans.entity.Customer;
import co.com.tienda.tienda.model.repository.CustomerRepository;
import co.com.tienda.tienda.model.service.CustomerService;

@Service("customerService")
@Transactional
public class CustomerServiceImpl implements CustomerService{
	
	@Autowired
	private CustomerRepository customerRepo;

	@Override
	public Customer findCustomerById(long customerId) {
		return customerRepo.findOne(customerId);
	}

	@Override
	public List<Customer> findAllCustomers() {
		return customerRepo.findAll();
	}

}
