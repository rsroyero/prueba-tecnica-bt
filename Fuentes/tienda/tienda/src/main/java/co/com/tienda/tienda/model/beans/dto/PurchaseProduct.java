package co.com.tienda.tienda.model.beans.dto;

import java.io.Serializable;

public class PurchaseProduct implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private long productId;
	
	private int productAmount;
	
	private String productName;
	
	public PurchaseProduct() {

	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public int getProductAmount() {
		return productAmount;
	}

	public void setProductAmount(int productAmount) {
		this.productAmount = productAmount;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	

}
