package co.com.tienda.tienda.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.tienda.tienda.model.beans.entity.Customer;
import co.com.tienda.tienda.model.beans.entity.ProductAvailableCustomer;

public interface ProductAvailableCustomerRepository extends JpaRepository<ProductAvailableCustomer,Long>{

	List<ProductAvailableCustomer> findByCustomer(Customer customer);
}
