package co.com.tienda.tienda.model.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.tienda.tienda.model.beans.dto.PurchaseOrderRecordDTO;
import co.com.tienda.tienda.model.beans.dto.PurchaseOrderReportDTO;
import co.com.tienda.tienda.model.beans.dto.PurchaseProduct;
import co.com.tienda.tienda.model.beans.entity.Customer;
import co.com.tienda.tienda.model.beans.entity.PurchaseOrder;
import co.com.tienda.tienda.model.beans.entity.PurchaseOrderDetail;
import co.com.tienda.tienda.model.repository.PurchaseOrderDetailRepository;
import co.com.tienda.tienda.model.repository.PurchaseOrderRepository;
import co.com.tienda.tienda.model.service.CustomerService;
import co.com.tienda.tienda.model.service.ProductService;
import co.com.tienda.tienda.model.service.PurchaseOrderService;

@Service("purchaseOrderService")
@Transactional
public class PurchaseOrderServiceImpl implements PurchaseOrderService{
	
	@Autowired
	private PurchaseOrderRepository purchaseOrderRepo;
	@Autowired
	private PurchaseOrderDetailRepository purchaseOrderDetailRepo;
	@Autowired
	private CustomerService customerService;
	@Autowired
	private ProductService productService;
	

	@Override
	public void createPurchaseOrder(PurchaseOrderRecordDTO purchaseOrderRecordDTO) {
		
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		Customer customer = customerService.findCustomerById(purchaseOrderRecordDTO.getCustomerId());
		purchaseOrder.setCustomer(customer);
		purchaseOrder.setCreationDate(new Date());
		purchaseOrder.setTotalPrice(purchaseOrderRecordDTO.getTotalPrice());
		purchaseOrder.setDeliveryAddress(purchaseOrderRecordDTO.getDeliveryAddress());
		
		purchaseOrderRepo.saveAndFlush(purchaseOrder);
		
		for (PurchaseProduct purchaseProduct : purchaseOrderRecordDTO.getProductList()) {
			PurchaseOrderDetail purchaseOrderDetail = new PurchaseOrderDetail();
			purchaseOrderDetail.setPurchaseOrder(purchaseOrder);

			purchaseOrderDetail.setProduct(productService.findByProductId(purchaseProduct.getProductId()));
			purchaseOrderDetail.setProductAmount(purchaseProduct.getProductAmount());
			purchaseOrderDetailRepo.saveAndFlush(purchaseOrderDetail);
		}

	}

	@Override
	public List<PurchaseOrderReportDTO> findPurchaseOrderReport(long customerId, Date beforeDate, Date afterDate) {
		
		List<PurchaseOrderReportDTO> purchaseOrderReportDTOList = new ArrayList<>();
		Customer customer = customerService.findCustomerById(customerId);
		List<PurchaseOrder> purchaseOrderList= purchaseOrderRepo.findByCustomerAndCreationDateBetween(customer, beforeDate, afterDate);

		for (PurchaseOrder purchaseOrder : purchaseOrderList) {
			PurchaseOrderReportDTO purchaseOrderReportDTO = new PurchaseOrderReportDTO();
			purchaseOrderReportDTO.setPurchaseOrder(purchaseOrder);
			List<PurchaseOrderDetail> purchaseOrderDetailList =purchaseOrderDetailRepo.findByPurchaseOrder(purchaseOrder);
			List<PurchaseProduct> PurchaseProductList = new ArrayList<>();
			for(PurchaseOrderDetail purchaseOrderDetail: purchaseOrderDetailList) {
				PurchaseProduct purchaseProduct = new PurchaseProduct();
				purchaseProduct.setProductAmount(purchaseOrderDetail.getProductAmount());
				purchaseProduct.setProductId(purchaseOrderDetail.getProduct().getProductId());
				purchaseProduct.setProductName(purchaseOrderDetail.getProduct().getName());
				PurchaseProductList.add(purchaseProduct);
			}
			purchaseOrderReportDTO.setPurchaseProductList(PurchaseProductList);
			purchaseOrderReportDTOList.add(purchaseOrderReportDTO);
		}
		return purchaseOrderReportDTOList;
	}

}
