package co.com.tienda.tienda.model.beans.dto;

import java.util.List;

public class PurchaseOrderRecordDTO {
	
	private long customerId;
	
	private String deliveryAddress;
	
	private List<PurchaseProduct> productList;
	
	private double totalPrice;

	public PurchaseOrderRecordDTO() {

	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public List<PurchaseProduct> getProductList() {
		return productList;
	}

	public void setProductList(List<PurchaseProduct> productList) {
		this.productList = productList;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	
	
}
