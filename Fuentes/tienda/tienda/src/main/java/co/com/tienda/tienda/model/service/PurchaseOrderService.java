package co.com.tienda.tienda.model.service;

import java.util.Date;
import java.util.List;

import co.com.tienda.tienda.model.beans.dto.PurchaseOrderReportDTO;
import co.com.tienda.tienda.model.beans.dto.PurchaseOrderRecordDTO;

public interface PurchaseOrderService {
	
	void createPurchaseOrder(PurchaseOrderRecordDTO purchaseOrderRecordDTO);
	
	List<PurchaseOrderReportDTO> findPurchaseOrderReport(long customerId, Date beforeDate, Date afterDate);
	
	

}
