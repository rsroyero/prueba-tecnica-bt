package co.com.tienda.tienda.model.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.tienda.tienda.model.beans.entity.Customer;
import co.com.tienda.tienda.model.beans.entity.Product;
import co.com.tienda.tienda.model.beans.entity.ProductAvailableCustomer;
import co.com.tienda.tienda.model.repository.ProductAvailableCustomerRepository;
import co.com.tienda.tienda.model.repository.ProductRepository;
import co.com.tienda.tienda.model.service.ProductService;


@Service("productService")
@Transactional
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	private ProductRepository productRepo;
	
	@Autowired
	private ProductAvailableCustomerRepository productAvailableCustomerRepo;

	@Override
	public Product findByProductId(long productId) {
		return productRepo.findOne(productId);
	}

	@Override
	public List<Product> findProductsByCustomer(Customer customer) {
		List<Product> productList = new ArrayList<>();
		for(ProductAvailableCustomer pac : productAvailableCustomerRepo.findByCustomer(customer)) {
			productList.add(pac.getProduct());
		}
		return productList;
	}


}
