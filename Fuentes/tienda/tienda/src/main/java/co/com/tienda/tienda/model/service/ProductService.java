package co.com.tienda.tienda.model.service;

import java.util.List;

import co.com.tienda.tienda.model.beans.entity.Customer;
import co.com.tienda.tienda.model.beans.entity.Product;

public interface ProductService {
	
	Product findByProductId(long productId);
	
	List<Product> findProductsByCustomer(Customer customer);

}
