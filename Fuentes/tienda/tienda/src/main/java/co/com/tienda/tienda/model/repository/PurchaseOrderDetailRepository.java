package co.com.tienda.tienda.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.tienda.tienda.model.beans.entity.PurchaseOrder;
import co.com.tienda.tienda.model.beans.entity.PurchaseOrderDetail;

public interface PurchaseOrderDetailRepository extends JpaRepository<PurchaseOrderDetail,Long>{
	
	List<PurchaseOrderDetail> findByPurchaseOrder(PurchaseOrder purchaseOrder);

}
