package co.com.tienda.tienda.model.beans.dto;

import java.io.Serializable;
import java.util.List;

import co.com.tienda.tienda.model.beans.entity.PurchaseOrder;

public class PurchaseOrderReportDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private PurchaseOrder purchaseOrder;
	
	private List<PurchaseProduct> PurchaseProductList;
	

	public PurchaseOrderReportDTO() {
	}
	
	
	public PurchaseOrder getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}


	public List<PurchaseProduct> getPurchaseProductList() {
		return PurchaseProductList;
	}


	public void setPurchaseProductList(List<PurchaseProduct> purchaseProductList) {
		PurchaseProductList = purchaseProductList;
	}
	
	
	
}
