package co.com.tienda.tienda.model.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.tienda.tienda.model.beans.entity.Customer;
import co.com.tienda.tienda.model.beans.entity.PurchaseOrder;

public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder,Long>{
	
	List<PurchaseOrder> findByCustomerAndCreationDateBetween(Customer customer, 
															 Date beforeDate, 
															 Date afterDate);

}
