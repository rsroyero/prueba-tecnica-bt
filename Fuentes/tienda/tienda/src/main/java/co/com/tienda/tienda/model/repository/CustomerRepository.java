package co.com.tienda.tienda.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.tienda.tienda.model.beans.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer,Long>{

}
