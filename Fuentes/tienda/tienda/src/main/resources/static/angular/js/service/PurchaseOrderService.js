'use strict';

App.factory('PurchaseOrderService', ['$http', '$q', function($http, $q){
	return{
	
		fetchAllProductsByCustomer: function(customerId){
			var deferred = $q.defer();
			$http.get('http://localhost:8080/api/all-products-customer/'+ customerId)
						.then(
								function(response){		
									deferred.resolve(response.data);
								},
								function(errResponse){
									console.error('Error while fetching Currencies');
									return $q.reject(errResponse);
								}
						);
			return deferred.promise;
		},
		
		createPurchaseOrder: function(purchaseOrderRecordDTO){
	        var deferred = $q.defer();
	        console.log(purchaseOrderRecordDTO);
	        $http.post('http://localhost:8080/api/purchase-order/', purchaseOrderRecordDTO)
	            .then(
	                function (response) {
	                    deferred.resolve(response.data);
	                },
	                function (errResponse) {
	                   console.error('Error while fetching Currencies');
	                   deferred.reject(errResponse);
	                }
	            );
	        return deferred.promise;
	    },
	    
	    findPurchaseOrderReport: function(customerId,beforeDate,afterDate){
	    	var deferred = $q.defer();
			$http.get('http://localhost:8080/api/purchase-order-report/' + customerId+'/'+beforeDate+'/'+afterDate)
						.then(
								function(response){		
									deferred.resolve(response.data);
								},
								function(errResponse){
									console.error('Error while fetching Currencies');
									return $q.reject(errResponse);
									
								}
						);
			return deferred.promise;
		}
		
	
		};			
	
	 
}]);
