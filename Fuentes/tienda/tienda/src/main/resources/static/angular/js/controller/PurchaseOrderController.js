'use strict';

App.controller('PurchaseOrderController',['$scope', 'PurchaseOrderService', function($scope, PurchaseOrderService){
	var self = this;
	self.totalPrice=null;
	self.customer={customerId:null, name:'', email:''};
	self.product={productId:null, name:'', price:null};
	self.productToPurchase={productId:null, name:'', totalPrice:null, amount:null};
	self.createPurchaseOrderDTO={customerId:null, deliveryAddress:'', productList:[], totalPrice:null};
	self.purchaseProduct={productId:null, productAmount:null};
	self.products=[];
	self.productToPurchases=[];
	self.productList=[];
	self.purchaseOrders=[];
	self.viewReport=false;
	self.viewPurchaseOrder=false;
	self.message='';
	self.purchaseMessage='';
	self.totalAmount=null;
	

	$scope.selectedCustomer = function(customer) {
		self.customer=customer;
		self.viewPurchaseOrder=false;
		self.viewReport=true;
		self.purchaseOrders=[];
		self.message='';
	};
	    
    

	$scope.fetchAllProductsByCustomer = function(customer) {
		self.viewPurchaseOrder=true;
		self.viewReport=false;
		self.customer = customer;
		self.products = [];
		self.totalAmount= null;
		self.message='';
		self.purchaseMessage='';
		self.productToPurchases=[];
		self.totalAmount=null;
		PurchaseOrderService.fetchAllProductsByCustomer(customer.customerId)
		.then(
				function(data){
					self.products=data;
				},
				function(errResponse){
					console.error(errResponse.data.errorMessage);
				}
			 
		);
    };
    
	$scope.addProductToPurchase = function(product,amount) {
		self.message='';
		if(amount!==null && amount>0 && amount<=5){
			
			self.totalAmount= self.totalAmount +amount;
			if(self.totalAmount>5){
				self.message='Ya Sobre paso la cantidad maxima de 5 productos';
				   return;
			}
			var inArray = false;
			   for ( var i=0; i < self.productToPurchases.length; i++) {

				   	  if(self.productToPurchases[i].productId == product.productId){
				   		inArray = true;
				   		self.productToPurchases[i].amount = amount;
				   		self.productToPurchases[i].totalPrice = product.price * amount;
				   	  }
				    };
			if(!inArray){
				self.productToPurchase = {};
				self.productToPurchase.productId = product.productId;
				self.productToPurchase.name = product.name;
				self.productToPurchase.totalPrice = product.price * amount;
				self.productToPurchase.amount = amount;
				self.productToPurchases.push(self.productToPurchase);
			}
			
			$scope.amountt=null;
			calculateTotalPrice();
		}else{
			self.message='La Cantidad de productos no puede ser vacia, 0 y no debe pasar el monto maximo de 5 Productos';
		}

    };
    
	$scope.removeProductToPurchase = function(productId) {
	   for ( var i=0; i < self.productToPurchases.length; i++) { 
		   	  if(self.productToPurchases[i].productId == productId){
		   		self.productToPurchases.splice(i,1);
		   	  }
		    };
		    calculateTotalPrice();
    };
    
    
    function calculateTotalPrice() {
    	self.totalPrice=null;
 	   for ( var i=0; i < self.productToPurchases.length; i++) { 
 		   self.totalPrice = self.totalPrice + self.productToPurchases[i].totalPrice;
		    };
    }
    
    
	$scope.createPurchaseOrder = function(deliveryAddress) {
		self.message='';
		self.purchaseMessage='';
		if(deliveryAddress!=null && deliveryAddress!==''){
		 	   for ( var i=0; i < self.productToPurchases.length; i++) {
			 		  self.purchaseProduct={};
			 		    self.purchaseProduct.productId = self.productToPurchases[i].productId;
				 		self.purchaseProduct.productAmount =self.productToPurchases[i].amount;
				 		self.productList.push(self.purchaseProduct);

					    };
				self.createPurchaseOrderDTO.customerId = self.customer.customerId;
				self.createPurchaseOrderDTO.deliveryAddress = deliveryAddress;
				self.createPurchaseOrderDTO.productList = self.productList;
				self.createPurchaseOrderDTO.totalPrice = self.totalPrice;
				PurchaseOrderService.createPurchaseOrder(self.createPurchaseOrderDTO)
		        .then(
		            function (response) {
		                self.purchaseMessage='Compra realida con éxito';
		            },
		            function (errResponse) {
		            	console.error(errResponse.data.errorMessage);
		            }
		        );
		}else{
			self.message='Debe ingresar una direcceción';
		}


    };
    
	$scope.findPurchaseOrderReport = function(beforeDate,afterDate) {
		self.message='';
		self.purchaseOrders=[];

		var newBeforeDate = (beforeDate.getDate() + '-' + (beforeDate.getMonth() + 1) + '-' +  beforeDate.getFullYear());
		var newAfterDate = (afterDate.getDate() + '-' + (afterDate.getMonth() + 1) + '-' +  afterDate.getFullYear());
		
		PurchaseOrderService.findPurchaseOrderReport(self.customer.customerId, newBeforeDate, newAfterDate)
		.then(
				function(data){
					self.purchaseOrders=data;
				},
				function(errResponse){
					self.message='El Cliente no tiene compras realizadas';
					console.error(errResponse.data.errorMessage);
					
				}
			 	   
		);
		

		
    };
    
    $scope.formatDate = function(date){
        var dateOut = new Date(date);
        return dateOut;
  };
	

	
}]);