'use strict';

App.controller('PrincipalController',['$scope', 'PrincipalService', function($scope, PrincipalService){
	var self = this;
	self.customer={customerId:null, name:'', email:''};
	self.customers=[];

	self.fetchAllCustomers = function(){
		PrincipalService.fetchAllCustomers()
			.then(
					function(data){
						self.customers=data;
					},
					function(errResponse){
						console.error(errResponse.data.errorMessage);
					}
				 
			);
	},
	
	
	self.fetchAllCustomers();
	
}]);