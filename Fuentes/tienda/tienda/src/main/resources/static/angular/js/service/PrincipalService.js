'use strict';

App.factory('PrincipalService', ['$http', '$q', function($http, $q){
	return{

		fetchAllCustomers: function(){
			var deferred = $q.defer();
			$http.get('http://localhost:8080/api/all-customers')
						.then(
								function(response){	
									deferred.resolve(response.data);
								},
								function(errResponse){
									console.error('Error while fetching Currencies');
									return $q.reject(errResponse);
								}
						);
			return deferred.promise;
		}
	};
	 
}]);
