/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.5.13 : Database - tienda
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`tienda` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `tienda`;

/*Table structure for table `customer` */

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `customer_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `customer` */

insert  into `customer`(`customer_id`,`name`,`email`) values (1,'Manny Bharma','manny_bharma@gmail.com'),(2,'Alan Briggs','alan_briggs@gmail.com'),(3,'Alicia Fuentes','alicia_fuentes@gmail.com'),(4,'Yukki Kanato','yukki_kanato@gmail.com');

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `product_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `product` */

insert  into `product`(`product_id`,`name`,`price`) values (1,'Play Station 4',900000),(2,'Televisor Sony',150000),(3,'PC Master 15',600000),(4,'Escritorio',450000),(5,'MotoG 5',350000),(6,'MacBook Pro',1000000),(7,'Tenis 42',50000),(8,'Cargador USB',10000);

/*Table structure for table `product_available_customer` */

DROP TABLE IF EXISTS `product_available_customer`;

CREATE TABLE `product_available_customer` (
  `product_available_customer_id` int(10) NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  PRIMARY KEY (`product_available_customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `product_available_customer` */

insert  into `product_available_customer`(`product_available_customer_id`,`customer_id`,`product_id`) values (1,1,1),(2,1,2),(3,1,3),(4,2,4),(5,2,5),(6,2,6),(7,3,7),(8,3,8),(9,4,1),(10,4,2),(11,4,3),(12,4,4),(13,4,5),(14,4,6),(15,4,7),(16,4,8);

/*Table structure for table `purchase_order` */

DROP TABLE IF EXISTS `purchase_order`;

CREATE TABLE `purchase_order` (
  `purchase_order_id` int(10) NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) NOT NULL,
  `delivery_address` varchar(255) NOT NULL,
  `creation_date` date NOT NULL,
  `total_price` double NOT NULL,
  PRIMARY KEY (`purchase_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `purchase_order` */

/*Table structure for table `purchase_order_detail` */

DROP TABLE IF EXISTS `purchase_order_detail`;

CREATE TABLE `purchase_order_detail` (
  `purchase_order_detail_id` int(10) NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `product_amount` int(11) NOT NULL,
  PRIMARY KEY (`purchase_order_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `purchase_order_detail` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
